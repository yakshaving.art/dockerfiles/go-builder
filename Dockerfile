# This is a multistage build.

# Helper image for installing tools we need
# hadolint ignore=DL3007
FROM registry.gitlab.com/yakshaving.art/dockerfiles/base:master AS installer

# This is a multiarch image
ARG TARGETOS
ARG TARGETARCH

SHELL ["/bin/bash", "-eufo", "pipefail", "-c"]

# hadolint ignore=DL3003,DL3018,DL4006
RUN apk -qq --no-cache add \
		binutils \
		gcc \
		go \
		gzip \
		musl-dev \
		openssl \
		tar \
		xz \
	# install latest golang
	&& GOLANG_VER_SUM="$(curl -qsSL https://go.dev/dl/?mode=json | jq -r '[.[] | select (.stable==true) | .files[] | select (.filename | endswith(".src.tar.gz") )][0] | "\(.version):\(.sha256)"')" \
	&& export GOLANG_VER_SUM \
	&& curl -qsSL "https://golang.org/dl/${GOLANG_VER_SUM%%:*}.src.tar.gz" > go.tgz \
	&& ( echo "${GOLANG_VER_SUM##*:}  go.tgz" | sha256sum --check --status --strict ) \
	&& tar -C /tmp/ -xzf go.tgz \
	&& rm -f go.tgz \
	&& ( cd /tmp/go/src ; ./make.bash ) \
	&& rm -rf /tmp/go/pkg/bootstrap /tmp/go/pkg/obj \
	# install our tools
	&& bash /usr/local/bin/unhoard.sh golangci-lint "${TARGETOS}" "${TARGETARCH}" \
	&& bash /usr/local/bin/unhoard.sh goreleaser "${TARGETOS}" "${TARGETARCH}" \
	# install else
	&& echo "done"

# This is our final image, based on base image
FROM registry.gitlab.com/yakshaving.art/dockerfiles/base:master

LABEL maintaner="Ilya A. Frolov <if+gitlab@solas.is>" \
      description="Image we use as a builder image for go tools"

ENV GOROOT="/usr/local/go" \
    GOPATH="/go" \
    PATH="/go/bin:/usr/local/go/bin:$PATH"

# This is a multiarch image
ARG TARGETOS
ARG TARGETARCH

SHELL ["/bin/bash", "-eufo", "pipefail", "-c"]

COPY --from=installer --chown=root:root \
	/usr/local/bin/* \
	/usr/local/bin/

COPY --from=installer --chown=root:root \
	/tmp/go \
	/usr/local/go

# hadolint ignore=DL3018,SC2016
RUN \
	# We bind-mount scripts and tests so that we don't pollute the end image
	# NOTE: instead of mounting separate dirs, we mount both so that we don't have
	# empty /mnt/scripts and /mnt/tests directory leftovers in the image :ocd:
	--mount=type=bind,source=.gitlab.d/ci,target=/mnt,readonly \
	# We also bind-mount goss and trivy binaries from the docker-builder image
	--mount=type=bind,source=goss,target=/bin/goss,readonly \
	--mount=type=bind,source=trivy,target=/bin/trivy,readonly \
	# And we also bind-mount .trivyignore file
	--mount=type=bind,source=.trivyignore,target=/.trivyignore,readonly \
	# Main flow below
	apk add --no-cache \
		binutils \
		binutils-aarch64-none-elf \
		cosign \
		file \
		gawk \
		git \
		gnupg \
		libarchive-tools \
		make \
		musl-dev \
		parallel \
		sed \
		tar \
		unzip \
		xz \
	&& ( set +f \
		&& chmod 0755 /usr/local/bin/* ) \
	# at this point, the _build_ is done, and we proceed to run _tests_ from within the image,
	# in order to abort the whole thing if they fail, and never push anything unsafe to the registry
	# these stages should be the last, and they should also be self-contained, i.e. do not install
	# any dependencies to not pollute the end image
	&& bash /mnt/scripts/skkrty_inside_build.sh \
	&& bash /mnt/scripts/tests_inside_build.sh \
	&& echo "done"

CMD ["/bin/bash"]
