#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# This is a wrapper around trivy invocation
set -EeufCo pipefail
IFS=$'\t\n'

# trivy defaults
export TRIVY_QUIET="${TRIVY_QUIET:-true}"

if [[ "true" == "${GITLAB_CI:-unset}" ]]; then
	# in case we decide to cache it on CI later
	export TRIVY_CACHE_DIR="${CI_PROJECT_DIR:-.}/.cache/trivy"
	export TRIVY_SKIP_DIRS="${CI_PROJECT_DIR:-.}/.cache"

	# output all the issues first
	trivy config --exit-code 0 .
	trivy fs --exit-code 0 .
	trivy image --exit-code 0 "${CI_REGISTRY}/${CI_PROJECT_PATH}:${CI_COMMIT_REF_SLUG}"

	# fail pipeline on critical and high issues
	trivy config --exit-code 42 --severity CRITICAL,HIGH .
	trivy fs --exit-code 43 --severity CRITICAL,HIGH .
	trivy image --exit-code 44 --severity CRITICAL,HIGH "${CI_REGISTRY}/${CI_PROJECT_PATH}:${CI_COMMIT_REF_SLUG}"
else
	# locally, we only care about simply running the checks
	trivy config .
	trivy fs .
fi
