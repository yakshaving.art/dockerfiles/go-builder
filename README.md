# go builder

Image to build go tools. Contains goreleaser and golangci-lint as well.

This image is multiarch, see os/architectures we build in .gitlab-ci.yml
